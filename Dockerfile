FROM httpd:2

RUN useradd -ms /bin/bash myexp

RUN apt update && apt -y install --no-install-recommends python3 python3-pip python3-setuptools
RUN mkdir -p /var/lock/apache2; chown daemon /var/lock/apache2 \
    && mkdir -p /data && chown daemon /data

ADD docker/httpd.conf /usr/local/apache2/conf/httpd.conf
ADD scripts/init.sh /

ADD requirements.txt /opt/myexpenses/
RUN pip3 install -r /opt/myexpenses/requirements.txt
COPY myexpenses/ /opt/myexpenses/

CMD ["sh", "-c", "/init.sh"]
