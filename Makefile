coverage:
	venv/bin/coverage report -m

unittests: 
	venv/bin/coverage run --source myexpenses -m unittest tests/test_*.py

.PHONY: integrationtests
integrationtests: 
	venv/bin/python3 -m unittest tests/itest_*.py

localintegrationtests: container integrationtests clean

container: clean
	podman run -d --rm --name myexppostgres -e POSTGRES_USER=myexpenses -e POSTGRES_PASSWORD=myexpenses -e POSTGRES_DB=myexpenses -p 127.0.0.1:5432:5432 postgres:11

clean:
	podman rm -f myexppostgres || true

venv-test: venv
	venv/bin/pip3 install -r requirements-test.txt

.PHONY: venv
venv:
	if [ ! -d "venv" ]; then python3 -m venv venv; fi
	venv/bin/pip3 install -r requirements.txt

venv-pylint: venv
	venv/bin/pip3 install pylint pylint-exit anybadge black

lint:
	venv/bin/pylint --output-format=text myexpenses
