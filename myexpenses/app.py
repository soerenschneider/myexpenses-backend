#!/usr/bin/env python3

import json
import os
import re
import logging
import time
import datetime

# pattern for directories containing myexpenses files
UUID_PATTERN = re.compile(r"^[\da-f]{8}-([\da-f]{4}-){3}[\da-f]{12}$", re.IGNORECASE)


class MyExpenses:
    """ Myexpenses backend. """

    def __init__(self, args, db):
        if not args:
            raise ValueError("No args supplied")
        self.args = args

        if not db:
            raise ValueError("No db implementation supplied")
        self.db = db

        self._quit = False

    @staticmethod
    def _fix_record(record):
        """ Performs some alterations of the record """
        if not record:
            return record

        # rename "uuid" to "id" field
        if "uuid" in record:
            record["id"] = record["uuid"]
            del record["uuid"]

        # convert unix timestamp to datetime and rename key
        if "timeStamp" in record:
            record["time_stamp"] = datetime.datetime.fromtimestamp(record["timeStamp"])
            del record["timeStamp"]

        # convert unix timestamp to datetime
        if "date" in record:
            record["date"] = datetime.datetime.fromtimestamp(record["date"])

        # rename key
        if "crStatus" in record:
            record["cr_status"] = record["crStatus"]
            del record["crStatus"]

        # split 'label' into category and subcategory
        if "label" in record.keys():
            split = record["label"].split(" : ")
            if len(split) > 1:
                record["category"] = split[0]
                record["subcategory"] = split[1]
            else:
                record["category"] = record["label"]
            del record["label"]

        return record

    def pre_process(self, records):
        ordered = dict()

        if not records:
            return ordered

        for record in records:
            record = self._fix_record(record)
            if not record["type"] in ordered:
                ordered[record["type"]] = list()
            ordered[record["type"]].append(record)

        return ordered

    def handle_update(self, ordered):
        if ordered and "updated" in ordered:
            try:
                updates = ordered["updated"]
                logging.info("Processing %d updates", len(updates))
                self.db.handle_update(updates)
                del ordered["updated"]
            except Exception as err:
                logging.error("Error while updating records: %s", err)

    def handle_created(self, ordered):
        if ordered and "created" in ordered:
            try:
                inserts = ordered["created"]
                logging.info("Processing %d creates", len(inserts))
                self.db.handle_create(inserts)
                del ordered["created"]
            except Exception as err:
                logging.error("Error while inserting records: %s", err)

    def handle_deleted(self, ordered):
        if ordered and "deleted" in ordered:
            try:
                deletes = ordered["deleted"]
                logging.info("Processing %d deletes", len(deletes))
                self.db.handle_delete(deletes)
                del ordered["deleted"]
            except Exception as err:
                logging.error("Error while deleting: %s", err)

    def store_records(self, records):
        """ Process records and write to the database. """
        logging.info("Processing %d records...", len(records))
        ordered = self.pre_process(records)
        del records

        self.handle_update(ordered)
        self.handle_created(ordered)
        self.handle_deleted(ordered)

        if len(ordered) > 0:
            logging.warning("Found unknown %s", ordered.keys())

    def quit(self):
        """ Signal the watcher to gracefully shut down. """
        self._quit = True

    @staticmethod
    def read_file_content(file_path):
        """ Reads and returns the JSON records from the file. """
        if not os.path.isfile(file_path):
            return None

        with open(file_path) as json_file:
            try:
                return json.load(json_file)
            except json.decoder.JSONDecodeError:
                logging.error("Not a valid json file: %s", file_path)

        return None

    @staticmethod
    def examine_dir(base_dir):
        """
        Traverses dir and tries to auto-discover the directories containing
        myexpenses files. Returns list of paths leading to the found dirs.
        """
        found_dirs = list()
        for root, subdirs, files in os.walk(base_dir):
            for subdir in subdirs:
                if UUID_PATTERN.match(subdir):
                    path = os.path.join(root, subdir)
                    found_dirs.append(path)

        return found_dirs

    def collect_records(self, collected_files):
        """ Reads and returns the JSON content from the given list of files. """
        records = list()

        if not collected_files:
            return records

        for f in collected_files:
            content = self.read_file_content(f)
            if content:
                records.extend(content)

        return records

    def collect_files(self, dirs, last_success=None):
        """ Returns all files in the given directories matching the regex and are newer then last_success. """
        collected_files = list()

        if not dirs:
            return collected_files

        for target in dirs:
            found_files = self.find_files(target, since=last_success)
            collected_files.extend(found_files)

        logging.info("Found %d files", len(collected_files))
        return collected_files

    def watch(self, base_dir):
        """
        Continuosly watches the base dir for new myexpenses files and automatically
        inserts them in to the backend.
        """
        myexp_dirs = self.examine_dir(base_dir)
        logging.info("Found dir(s): %s", myexp_dirs)

        last_success = None
        while not self._quit:
            try:
                now = datetime.datetime.now()
                collected_files = self.collect_files(myexp_dirs, last_success)
                if len(collected_files) > 0:
                    records = self.collect_records(collected_files)
                    self.store_records(records)

                last_success = now
                time.sleep(self.args.interval)
            except KeyboardInterrupt:
                logging.info("Quitting, bye. ")
                self.quit()
            except Exception as error:
                logging.error("Error occured while processing: %s", error)
                time.sleep(self.args.interval)

    @staticmethod
    def find_files(target_dir, since=None):
        """
        Finds all usable files within a directory. Optional parameter delta takes seconds as int. If set, it only returns files
        newer than x seconds.
        """
        if not target_dir:
            raise ValueError("No target dir set")

        if since is not None and not isinstance(since, datetime.datetime):
            raise ValueError("delta must be a datetime")

        if since is None:
            since = datetime.datetime.fromtimestamp(0)
            logging.info("Finding all files in '%s'", target_dir)
        else:
            logging.info("Finding files in '%s' newer than %s", target_dir, since)

        matching_files = list()
        for root, dirs, files in os.walk(target_dir):
            for fname in files:
                path = os.path.join(root, fname)
                stat = os.stat(path)
                mtime = datetime.datetime.fromtimestamp(stat.st_mtime)

                if mtime > since and re.match(r"_[0-9]+.*\.json", fname):
                    matching_files.append(path)

        return matching_files
