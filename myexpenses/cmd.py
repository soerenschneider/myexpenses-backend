import os
import logging

import configargparse

from myexpenses.db import PostgresBackend
from myexpenses.app import MyExpenses


def setup_logging(debug=False):
    """ Sets up the logging. """
    loglevel = logging.INFO
    if debug:
        loglevel = logging.DEBUG

    logging.basicConfig(
        level=loglevel, format="%(levelname)s\t %(asctime)s %(message)s"
    )
    logging.getLogger("apscheduler").setLevel(logging.WARNING)
    logging.getLogger("chardet.charsetprober").setLevel(logging.INFO)


def print_config(args):
    """ Prints configuration upon startup. """
    logging.info("myexpenses backend starting up, using configuration:")
    logging.info("dir=%s", args.dir)
    logging.info("host=%s", args.host)
    logging.info("user=%s", args.user)
    logging.info("db=%s", args.dbname)
    logging.info("interval=%s", args.interval)
    logging.info("verbose=%s", args.verbose)


def parse_args():
    """ Parser user supplied arguments """
    parser = configargparse.ArgumentParser(prog="myexpenses")

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--dir", "-d", action="store")

    parser.add_argument("--host", action="store", default=os.getenv("EXP_HOST"))
    parser.add_argument(
        "--user", action="store", default=os.getenv("EXP_USER", "myexpenses")
    )
    parser.add_argument("--password", action="store", default=os.getenv("EXP_PASS"))
    parser.add_argument(
        "--dbname", action="store", default=os.getenv("EXP_DB", "myexpenses")
    )
    parser.add_argument("--interval", action="store", type=int, default=60)
    parser.add_argument("--verbose", action="store", default=False)
    return parser.parse_args()


def run():
    """ Start it up """
    args = parse_args()
    setup_logging(args.verbose)
    print_config(args)
    database = PostgresBackend(args)
    exp = MyExpenses(args, database)
    exp.watch(args.dir)
