import os

import psycopg2
import backoff


class PostgresBackend:
    """ Interacts with the database. """

    def __init__(self, args):
        if not args:
            raise ValueError("No args given")

        self._args = args
        self._initialize_schema()

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_time=30)
    def _initialize_schema(self):
        with psycopg2.connect(
            host=self._args.host,
            user=self._args.user,
            password=self._args.password,
            dbname=self._args.dbname,
        ) as connection:
            with connection.cursor() as cursor:
                path = os.path.join(
                    os.path.abspath(os.path.dirname(__file__)), "postgres/01-schema.sql"
                )
                with open(path, "r") as sql_file:
                    cursor.execute(sql_file.read())

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_time=30)
    def handle_create(self, records, conn=None):
        """ Creates a record """
        with (
            conn
            if conn is not None
            else psycopg2.connect(
                host=self._args.host,
                user=self._args.user,
                password=self._args.password,
                dbname=self._args.dbname,
            )
        ) as connection:
            with connection.cursor() as cursor:
                for record in records:
                    sql = "INSERT INTO myexpenses VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING"
                    cursor.execute(
                        sql,
                        (
                            record["id"],
                            record["type"],
                            record["time_stamp"],
                            record.get("comment", None),
                            record["date"],
                            record["amount"],
                            record.get("category", None),
                            record.get("subcategory", None),
                            record["cr_status"],
                        ),
                    )

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_time=30)
    def handle_update(self, records, conn=None):
        """ Updates a record """
        with (
            conn
            if conn is not None
            else psycopg2.connect(
                host=self._args.host,
                user=self._args.user,
                password=self._args.password,
                dbname=self._args.dbname,
            )
        ) as connection:
            with connection.cursor() as cursor:
                for record in records:
                    # don't try to 'update' the id...
                    uuid = record["id"]
                    del record["id"]
                    # build list of affected columns dynamically
                    sql_template = (
                        "UPDATE myexpenses SET ({values}) = (VALUES %s) WHERE id = %s"
                    )
                    sql = sql_template.format(values=", ".join(record.keys()))

                    # execute..
                    cursor.execute(sql, (tuple(record.values()), uuid))

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_time=30)
    def handle_delete(self, records, conn=None):
        """ Delete a record. """
        with (
            conn
            if conn is not None
            else psycopg2.connect(
                host=self._args.host,
                user=self._args.user,
                password=self._args.password,
                dbname=self._args.dbname,
            )
        ) as connection:
            with connection.cursor() as cursor:
                for record in records:
                    if not "id" in record.keys() or not record["id"]:
                        return
                    sql = "DELETE FROM myexpenses WHERE id = %s"
                    cursor.execute(sql, (record["id"],))
