CREATE TABLE IF NOT EXISTS myexpenses (                                                    
    id UUID PRIMARY KEY,
    type TEXT,
    time_stamp TIMESTAMP,
    comment TEXT,
    date TIMESTAMP,
    amount INT,
    category TEXT,
    subcategory TEXT,
    cr_status TEXT
);

CREATE INDEX IF NOT EXISTS myexpenses_category ON myexpenses USING HASH (category);
CREATE INDEX IF NOT EXISTS myexpenses_date ON myexpenses (date);
