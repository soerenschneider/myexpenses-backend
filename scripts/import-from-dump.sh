#!/bin/sh

if [ -z ${1} ]; then
    echo "Please provide sql file"
    exit 1
fi

if [ -z ${2} ]; then
    echo "Please provide docker container"
    exit 1
fi


cat "${1}" | docker exec -i ${2} psql -U myexpenses  -d myexpenses
