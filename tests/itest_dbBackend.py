import unittest
from unittest import TestCase
import json
import os

import argparse
import psycopg2
from psycopg2.extras import RealDictCursor
import backoff
import logging
from myexpenses.app import MyExpenses
from myexpenses.db import PostgresBackend

class TestPostgresBackend(TestCase):
    host=os.getenv("POSTGRES_HOST", "localhost")
    user=os.getenv("POSTGRES_DB", "myexpenses")
    pw=os.getenv("POSTGRES_USER", "myexpenses")
    db=os.getenv("POSTGRES_PASSWORD", "myexpenses")
    args = argparse.Namespace(host=host, user=user, password=pw, dbname=db)

    @staticmethod
    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_time=30)
    def setUpClass():
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s\t %(asctime)s %(message)s")

        db = psycopg2.connect(host=TestPostgresBackend.host, user=TestPostgresBackend.user, password=TestPostgresBackend.pw, dbname=TestPostgresBackend.db)
        with db.cursor() as cursor:
            with open('myexpenses/postgres/01-schema.sql','r') as sql_file:
                cursor.execute(sql_file.read())
        db.commit()
        db.close()

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_time=30)
    def setUp(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor() as cursor:
            sql = "DELETE FROM myexpenses"
            cursor.execute(sql)
        db.commit()
        db.close()

    def test_create(self):
        # prepare
        db = PostgresBackend(self.args)
        exp = MyExpenses(self.args, db)
        raw_records = """[{"type":"created","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","timeStamp":1557695029,"comment":"Burger","date":1557666180,"amount":-1000,"label":"Food : Restaurant","crStatus":"UNRECONCILED"}]"""
        records = json.loads(raw_records)

        # precondition
        persisted_records = self.read_records()
        self.assertEqual(0, len(persisted_records))

        # process
        exp.store_records(records)

        # verify
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))

    def test_update_existing_record(self):
        # prepare
        db = PostgresBackend(self.args)
        exp = MyExpenses(self.args, db)
        raw_records_initial = """[{"type":"created","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","timeStamp":1557695029,"comment":"Burger","date":1557666180,"amount":-1000,"label":"Food : Restaurant","crStatus":"UNRECONCILED"}]"""
        records = json.loads(raw_records_initial)

        # precondition
        persisted_records = self.read_records()
        self.assertEqual(0, len(persisted_records))

        # create record
        exp.store_records(records)

        # check current state
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))
        self.assertEqual(-1000, persisted_records[0]["amount"])

        # perform update
        raw_records_update = """[{"type":"updated","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","amount":-1100}]"""
        records = json.loads(raw_records_update)
        exp.store_records(records)

        # verify
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))
        self.assertEqual(-1100, persisted_records[0]["amount"])
    
    def test_update_nonexisting_record(self):
        # prepare
        db = PostgresBackend(self.args)
        exp = MyExpenses(self.args, db)
        raw_records_initial = """[{"type":"created","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","timeStamp":1557695029,"comment":"Burger","date":1557666180,"amount":-1000,"label":"Food : Restaurant","crStatus":"UNRECONCILED"}]"""
        records = json.loads(raw_records_initial)

        # precondition
        persisted_records = self.read_records()
        self.assertEqual(0, len(persisted_records))

        # create record
        exp.store_records(records)

        # check current state
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))
        self.assertEqual(-1000, persisted_records[0]["amount"])

        # perform update
        raw_records_update = """[{"type":"updated","uuid":"ffffffff-6060-4bb2-89c7-99e08fbf564a","amount":-1100}]"""
        records = json.loads(raw_records_update)
        exp.store_records(records)

        # verify
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))
        self.assertEqual(-1000, persisted_records[0]["amount"])

    def test_delete_existing_record(self):
        # prepare
        db = PostgresBackend(self.args)
        exp = MyExpenses(self.args, db)
        raw_records_initial = """[{"type":"created","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","timeStamp":1557695029,"comment":"Burger","date":1557666180,"amount":-1000,"label":"Food : Restaurant","crStatus":"UNRECONCILED"}]"""
        records = json.loads(raw_records_initial)

        # precondition
        persisted_records = self.read_records()
        self.assertEqual(0, len(persisted_records))

        # create record
        exp.store_records(records)

        # check current state
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))

        # perform delete
        raw_records_update = """[{"type":"deleted","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a"}]"""
        records = json.loads(raw_records_update)
        exp.store_records(records)

        # verify
        persisted_records = self.read_records()
        self.assertEqual(0, len(persisted_records))

    def test_delete_non_existing_record(self):
        # prepare
        db = PostgresBackend(self.args)
        exp = MyExpenses(self.args, db)
        raw_records_initial = """[{"type":"created","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","timeStamp":1557695029,"comment":"Burger","date":1557666180,"amount":-1000,"label":"Food : Restaurant","crStatus":"UNRECONCILED"}]"""
        records = json.loads(raw_records_initial)

        # precondition
        persisted_records = self.read_records()
        self.assertEqual(0, len(persisted_records))

        # create record
        exp.store_records(records)

        # check current state
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))

        # perform delete
        raw_records_update = """[{"type":"deleted","uuid":"ffffffff-6060-4bb2-89c7-99e08fbf564a"}]"""
        records = json.loads(raw_records_update)
        exp.store_records(records)

        # verify
        persisted_records = self.read_records()
        self.assertEqual(1, len(persisted_records))

    def read_records(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor(cursor_factory=RealDictCursor) as cursor:
            sql = f"SELECT * FROM myexpenses"
            cursor.execute(sql)
            return cursor.fetchall()

if __name__ == '__main__':
    unittest.main()
