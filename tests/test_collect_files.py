from unittest import TestCase
from myexpenses.app import MyExpenses
import logging
import json
import datetime
import argparse

class DbDummy():
    pass

class TestFiles(TestCase):
    @staticmethod
    def setUpClass():
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s\t %(asctime)s %(message)s"
    )

    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)

    def test_collect_files_empty(self):
        dirs = list()
        records = self.exp.collect_files(dirs)
        self.assertEqual(0, len(records))

    def test_collect_files_none(self):
        dirs = None
        records = self.exp.collect_files(dirs)
        self.assertEqual(0, len(records))

    def test_collect_files_simple(self):
        dirs = ["testdata/test_examine_dir/test_1/nested/deeply/789d54d7-02aa-452b-9415-8d993609bea2"]
        expected_files = [
            "testdata/test_examine_dir/test_1/nested/deeply/789d54d7-02aa-452b-9415-8d993609bea2/_1.json",
            "testdata/test_examine_dir/test_1/nested/deeply/789d54d7-02aa-452b-9415-8d993609bea2/_2.json"
        ]
        files = self.exp.collect_files(dirs)
        self.assertEqual(2, len(files))

        for expected_file in expected_files:
            self.assertTrue(expected_file in files)

    def test_collect_files_simple_delta(self):
        dirs = ["testdata/test_examine_dir/test_1/nested/deeply/789d54d7-02aa-452b-9415-8d993609bea2"]
        files = self.exp.collect_files(dirs, datetime.datetime.now())
        self.assertEqual(0, len(files))