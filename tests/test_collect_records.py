from unittest import TestCase
from myexpenses.app import MyExpenses

import json
import argparse

class DbDummy():
    pass

class TestFiles(TestCase):
    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)

    def test_collect_records(self):
        files = ["testdata/_1.json"]
        records = self.exp.collect_records(files)
        self.assertEqual(2, len(records))
        self.assertEqual("ab993220-0900-48a0-ac0f-5aad64307254", records[0]["uuid"])
        self.assertEqual("c27b62d5-f499-4fb3-aa69-23db947fe775", records[1]["uuid"])

    def test_collect_records_empty(self):
        files = list()
        records = self.exp.collect_records(files)
        self.assertEqual(0, len(records))

    def test_collect_records_none(self):
        files = None
        records = self.exp.collect_records(files)
        self.assertEqual(0, len(records))

    def test_collect_records_invalid(self):
        files = ["testdata/_2.json"]
        records = self.exp.collect_records(files)
        self.assertEqual(0, len(records))