from unittest import TestCase
from myexpenses.app import MyExpenses

import json
import argparse

class DbDummy():
    pass

class TestFiles(TestCase):
    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)

    def test_examine_dir_test_1(self):
        arg = "testdata/test_examine_dir/test_1"
        expected_dir = "testdata/test_examine_dir/test_1/nested/deeply/789d54d7-02aa-452b-9415-8d993609bea2"
        dirs = self.exp.examine_dir(arg)
        self.assertEqual(1, len(dirs))
        self.assertEqual(expected_dir, dirs[0])

    def test_examine_dir_test_2(self):
        arg = "testdata/test_examine_dir/test_2"
        expected_dirs = list()
        dirs = self.exp.examine_dir(arg)
        self.assertEqual(2, len(dirs))
        for expected in expected_dirs:
            if expected_dirs not in dirs:
                self.fail("expected dir '%s' not found in dirs", expected)

    def test_examine_dir_test_3(self):
        arg = "testdata/test_examine_dir/test_3"
        dirs = self.exp.examine_dir(arg)
        self.assertEqual(0, len(dirs))