import os
import json
import shutil
import argparse
import datetime

from unittest import TestCase
from myexpenses.app import MyExpenses

class DbDummy():
    pass

class TestFiles(TestCase):
    old_file = "testdata/test_find_files/nested/_1.json"
    new_file = "testdata/test_find_files/nested/_2.json"

    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)
        try:
            os.remove(self.new_file)
        except:
            pass

    def test_find_files(self):
        found_files = MyExpenses.find_files("testdata/test_find_files/")
        self.assertEqual(1, len(found_files))

    def test_find_files_delta_now(self):
        found_files = MyExpenses.find_files("testdata/test_find_files/", datetime.datetime.now())
        self.assertEqual(0, len(found_files))

    def test_find_files_delta_3s_ago(self):
        delta = datetime.datetime.now() - datetime.timedelta(seconds=3)
        found_files = MyExpenses.find_files("testdata/test_find_files/", delta)
        self.assertEqual(0, len(found_files))
        shutil.copyfile(self.old_file, self.new_file)
        found_files = MyExpenses.find_files("testdata/test_find_files/", delta)
        self.assertEqual(1, len(found_files))
        self.assertEqual(self.new_file, found_files[0])
