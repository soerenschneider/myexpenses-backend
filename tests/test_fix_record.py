import argparse
import datetime
from datetime import timezone

from unittest import TestCase
from myexpenses.app import MyExpenses

class DbDummy():
    pass

class Test_FixRecords(TestCase):
    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)

    def test_fix_record_empty(self):
        record = dict()
        fixed = self.exp._fix_record(record)
        print(fixed)

    def test_fix_record_none(self):
        record = None
        fixed = self.exp._fix_record(record)
        print(fixed)

    def test_fix_record_unknown_key(self):
        record = dict()
        val = "cool value"
        record["thiskeyisunknown"] = val
        fixed = self.exp._fix_record(record)
        self.assertEqual(val, fixed["thiskeyisunknown"])

    def test_fix_record_id(self):
        record = dict()
        uuid = "299807f4-c5f3-49fd-a5a1-c57768b04fac"
        record["uuid"] = uuid
        fixed = self.exp._fix_record(record)
        self.assertTrue("uuid" not in fixed)
        self.assertEqual(uuid, fixed["id"])
    
    def test_fix_record_label_no_subcategory(self):
        record = dict()
        label = "Label"
        record["label"] = label
        fixed = self.exp._fix_record(record)
        self.assertTrue("label" not in fixed)
        self.assertTrue("subcategory" not in fixed)
        self.assertEqual(label, fixed["category"])

    def test_fix_record_label(self):
        record = dict()
        category = "This is the category"
        subcategory = "This is the subcategory"
        label = "{} : {}".format(category, subcategory)
        record["label"] = label
        fixed = self.exp._fix_record(record)
        self.assertTrue("label" not in fixed)
        self.assertEqual(category, fixed["category"])
        self.assertEqual(subcategory, fixed["subcategory"])

    def test_fix_record_label_timestamp(self):
        record = dict()
        timestamp = 1572112894
        date = datetime.datetime.fromtimestamp(timestamp)
        record["timeStamp"] = timestamp
        fixed = self.exp._fix_record(record)
        self.assertTrue("timeStamp" not in fixed)
        self.assertEqual(date, fixed["time_stamp"])

    def test_fix_record_label_date(self):
        record = dict()
        timestamp = 1572112894
        date = datetime.datetime.fromtimestamp(timestamp)
        record["date"] = timestamp
        fixed = self.exp._fix_record(record)
        self.assertEqual(date, fixed["date"])
