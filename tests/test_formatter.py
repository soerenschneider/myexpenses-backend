from unittest import TestCase
from myexpenses.app import MyExpenses

import json
import argparse

class DbDummy():
    pass

class TestFormatter(TestCase):
    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)

    def test_fix_record_empty(self):
        raw_record = """[{}]"""
        record = json.loads(raw_record)
        self.exp._fix_record(record[0])

    def test_fix_record_populated(self):
        raw_record = """[{"type":"created","uuid":"c40f961e-6060-4bb2-89c7-99e08fbf564a","timeStamp":1557695029,"comment":"Burger","date":1557666180,"amount":-1000,"label":"Food : Restaurant","crStatus":"UNRECONCILED"}]"""
        record = json.loads(raw_record)
        self.exp._fix_record(record[0])


    def test_format_incident_3(self):
        raw_record = """[{"type":"updated","uuid":"4b2e3006-090f-458f-b8d1-3d3dc0d46af2","timeStamp":1562741772,"amount":-900}]"""
        raw_record = """[{"type":"updated","uuid":"942a5e72-5f68-448d-bbe7-0c6499992c0e","timeStamp":1562423608,"date":1562421900}]"""

    def test_multiple(self):
        raw_record = """[{"type":"created","uuid":"ab993220-0900-48a0-ac0f-5aad64307254","timeStamp":1571672411,"comment":"Kantine","date":1571655780,"amount":-500,"label":"Food : Canteen","crStatus":"UNRECONCILED"},{"type":"created","uuid":"c27b62d5-f499-4fb3-aa69-23db947fe775","timeStamp":1571672433,"date":1571657280,"amount":160,"label":"Food : Lunchit","crStatus":"UNRECONCILED"}]"""
