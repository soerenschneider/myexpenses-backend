import argparse
import datetime
from datetime import timezone

from unittest import TestCase
from myexpenses.app import MyExpenses

class DbDummy():
    pass

class Test_Preprocess(TestCase):
    def setUp(self):
        dummy = DbDummy()
        args = argparse.Namespace()
        self.exp = MyExpenses(args, dummy)

    def test_preprocess_empty(self):
        records = list()
        processed = self.exp.pre_process(records)
        self.assertEqual(0, len(processed))

    def test_preprocess_none(self):
        records = None
        processed = self.exp.pre_process(records)
        self.assertEqual(0, len(processed))

    def test_preprocess_single(self):
        records = list()

        record = dict()
        record["type"] = "a"
    
        records.append(record)
        
        processed = self.exp.pre_process(records)
        self.assertEqual(1, len(processed))

    def test_preprocess_multiple(self):
        records = list()

        record = dict()
        record["type"] = "one type"
        records.append(record)

        record = dict()
        record["type"] = "another type"
        records.append(record)
        
        processed = self.exp.pre_process(records)
        self.assertEqual(2, len(processed))
